package simulation;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

public class Test {
	static final int K_MIN = 2;
	static final int K_MAX = 8;
	static final int[] N = { 10, 15, 20 };
	
	static final double SIMULATION_TIME = 24.0; //in hours
	static final int[] ITERATIONS = { 10, 25, 100 };

	public static void main(String[] args) {
		Simulator s = new Simulator(K_MIN, K_MAX, N);
		
		try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("rezultati_simulacija.txt")))){
			s.calcResults(2, 10, SIMULATION_TIME, writer);
			/*for (int j = 0; j < N.length; j++)
				for (int k = K_MIN; k <= K_MAX; k++)
					for (int i = 1; i <= ITERATIONS[j] ; i++) {
						writer.write("----------------K=" + k + ", N=" + N[j] + ", ITERATION=" + i + "----------------\n");
						s.calcResults(k, N[j], SIMULATION_TIME, writer);
						writer.write("\n\n");
					}*/
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("rezultati_simulacija_usrednjeno.txt")))){
			for (int n: N)
				for (int k = K_MIN; k <= K_MAX; k++) {
					writer.write("----------------K=" + k + ", N=" + n + "----------------\n");
					s.calcAverageResults(k, n, writer);
					writer.write("\n\n");
				}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("Simulation successfully completed!");

	}

}
