package simulation;

import java.util.LinkedList;

public class Job {	
	private int server = 0; //na kom resursu se sada nalazi posao
	private double time_arrived;
	private double time_to_wake;
	private boolean calc_done_but_still_in_queue_for_old_server = false;
	
	private double[] job_spended_time_on_server; // koliko je vremena koji posao proveo na kom resursu
	private double[] job_spended_time_in_queue; // koliko je vremena koji posao proveo u kom redu za cekanje
	private long[] job_times_on_server; // koliko se puta koji posao izvrsio na kom resursu
	
	private int serial_number;
	private int k;
	private Simulator simulator;
	
	private double last_cycle_exec_time;
	private LinkedList<Double> cycle_durations;
	
	public Job(int serial_number, int k, double time_in_ms, Simulator simulator) {
		super();
		this.serial_number = serial_number;
		this.k = k;
		this.simulator = simulator;
		
		job_spended_time_on_server = new double [k+4];
		job_spended_time_in_queue = new double [k+4];
		job_times_on_server = new long [k+4];
		
		last_cycle_exec_time = time_in_ms;
		cycle_durations = new LinkedList<Double>();
	}

	public void calc(double current_time_in_ms) {
		double time_waited = time_arrived - current_time_in_ms;
		double time_executed = simulator.getExponentialDistributionSample(server);
		
		job_spended_time_in_queue[server] += time_waited;
		job_spended_time_on_server[server] += time_executed;
		job_times_on_server[server]++;
		
		time_to_wake = current_time_in_ms - time_executed;
	}
	
	private int getNextServer() {
		double value = simulator.getNextRandomValue();
		switch (server) {
		case 0:
			if (value <= 0.1)
				return 0;
			else if (value <= 0.2)
				return 1;
			else if (value <= 0.3)
				return 2;
			else if (value <= 0.4)
				return 3;
			else {
				value -= 0.4;
				value /= (60/(k * 100.0));
				value += 4;
				return (int) value;
			}
		case 1: case 2: case 3:
			if (value <= 0.4)
				return 0;
			else {
				value -= 0.4;
				value /= (60/(k * 100.0));
				value += 4;
				return (int) value;
			}
		default:
			return 0;
		}
	}
	
	public int getServer() {
		return server;
	}

	public void setNextServer() {
		server = getNextServer();
	}

	public int getSerial_number() {
		return serial_number;
	}

	public double[] get_Job_spended_time_on_server() {
		return job_spended_time_on_server;
	}
	
	public double[] get_Job_spended_time_in_queue() {
		return job_spended_time_in_queue;
	}
	
	public long[] get_Job_times_on_server() {
		return job_times_on_server;
	}

	public void setTime_arrived(double time_arrived) {
		this.time_arrived = time_arrived;
	}
	
	public double getTime_to_wake() {
		return time_to_wake;
	}

	public boolean is_Calc_done_but_still_in_queue_for_old_server() {
		return calc_done_but_still_in_queue_for_old_server;
	}

	public void set_Calc_done_but_still_in_queue_for_old_server(boolean calc_done_but_still_in_queue_for_old_server) {
		this.calc_done_but_still_in_queue_for_old_server = calc_done_but_still_in_queue_for_old_server;
	}
	
	public LinkedList<Double> get_Cycle_durations() {
		return cycle_durations;
	}

	public void end_cycle(double time_in_ms) {
		double duration = last_cycle_exec_time - time_in_ms;
		last_cycle_exec_time = time_in_ms;
		cycle_durations.add(duration);
	}

}
