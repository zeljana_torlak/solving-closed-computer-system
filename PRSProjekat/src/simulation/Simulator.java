package simulation;

import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Random;

import org.apache.commons.math3.distribution.ExponentialDistribution;

public class Simulator {
	static final double[] s = { 5.0, 20.0, 15.0, 15.0, 20.0 }; //ms
	private Random rand;
	private ExponentialDistribution[] ed;
	
	private LinkedList<LinkedList<Job>> queue;
	
	private HashMap<String, LinkedList<double[]>> Us;
	private HashMap<String, LinkedList<double[]>> Xps;
	private HashMap<String, LinkedList<double[]>> Js;
	private HashMap<String, LinkedList<Double>> Ts;
	
	public Simulator(int k_min, int k_max, int[] Ns) {
		int size = (k_max-k_min+1)*Ns.length;
		Us = new HashMap<String, LinkedList<double[]>>(size);
		Xps = new HashMap<String, LinkedList<double[]>>(size);
		Js = new HashMap<String, LinkedList<double[]>>(size);
		Ts = new HashMap<String, LinkedList<Double>>(size);
		
		for (int n: Ns)
			for (int k = k_min; k <= k_max; k++) {
				Us.put("" + k + " " + n, new LinkedList<double[]>());
				Xps.put("" + k + " " + n, new LinkedList<double[]>());
				Js.put("" + k + " " + n, new LinkedList<double[]>());
				Ts.put("" + k + " " + n, new LinkedList<Double>());
			}
		
		rand = new Random();
		ed = new ExponentialDistribution[s.length];
		for (int i = 0; i < s.length; i++)
			ed[i] = new ExponentialDistribution(s[i]);
	}

	void calcResults(int k, int n, double default_simulation_time_in_hours, Writer writer) throws IOException {
		double time_in_ms = default_simulation_time_in_hours * 60 * 60 * 1000;
		
		queue = new LinkedList<LinkedList<Job>>();
		for (int i = 0; i < (k+4); i++)
			queue.add(new LinkedList<Job>());

		double[][] number_of_jobs_on_server = new double[k+4][n+1]; //koliko dugo je bio odredjen broj procesa na kom serveru
		
		Job[] jobs = new Job[n];
		for (int i = 0; i < n; i++)
			jobs[i] = new Job(i, k, time_in_ms, this);
		 
		simulateRunningJobs(jobs, time_in_ms, number_of_jobs_on_server);	
		
		//toDo
		//System.out.println(AnalyticSolver.printMatrix(job_spended_time, "job_spended_time"));	
		writer.write(printArray(calcU(k, n, time_in_ms, jobs), "U"));
		writer.write(printArray(calcXp(k, n, time_in_ms, jobs), "X"));
		writer.write(printArray(calcJ(k, n, number_of_jobs_on_server), "J"));
		writer.write("T = " + String.format("%.3f [ms]", calcT(k, n, jobs)));	
	}
	
	private void simulateRunningJobs(Job[] jobs, double starting_time_in_ms, double[][] number_of_jobs_on_server) {
		double current_time_in_ms = starting_time_in_ms;
		for (int i = 0; i < jobs.length; i++) 
			addToQueue(jobs[i], current_time_in_ms);
		
		while (current_time_in_ms > 0) {
			LinkedList<Job> awakenedJobs = new LinkedList<Job>();
			for (int i = 0; i < queue.size(); i++)
				if (getFirstFromQueue(i) != null)
					awakenedJobs.add(getFirstFromQueue(i));
			
			if (awakenedJobs.isEmpty()) System.out.println("deadlock :)");
			
			for (int i = 0; i < awakenedJobs.size(); i++) {
				if (awakenedJobs.get(i).is_Calc_done_but_still_in_queue_for_old_server() == false) {
					awakenedJobs.get(i).calc(current_time_in_ms);
					awakenedJobs.get(i).set_Calc_done_but_still_in_queue_for_old_server(true);
				}
			}
			
			Job toWake = null;
			for (int i = 0; i < awakenedJobs.size(); i++) {
				if (toWake == null) 
					toWake = awakenedJobs.get(i);
				else {
					if (awakenedJobs.get(i).getTime_to_wake() > toWake.getTime_to_wake())
						toWake = awakenedJobs.get(i);
				}
			}
			
			double time_diff = current_time_in_ms;
			current_time_in_ms = toWake.getTime_to_wake();
			time_diff -= current_time_in_ms;

			for (int i = 0; i < number_of_jobs_on_server.length; i++)
				number_of_jobs_on_server[i][queue.get(i).size()] += time_diff;
			
			if (toWake.is_Calc_done_but_still_in_queue_for_old_server()) {
				removeFirstFromQueueAndAddToQueue(toWake, current_time_in_ms);
				toWake.set_Calc_done_but_still_in_queue_for_old_server(false);
			}
		}
		
	}
	
	private double[] calcU(int k, int n, double time_in_ms, Job[] jobs) {
		double[] U = new double[k+4];

		double[][] job_spended_time_on_server = new double [n][k+4];
		for (int i = 0; i < n; i++)
			job_spended_time_on_server[i] = jobs[i].get_Job_spended_time_on_server();

		for (int i = 0; i < (k+4); i++) {
			for (int j = 0; j < n; j++)
				U[i] += job_spended_time_on_server[j][i];
			U[i] /= time_in_ms;
		}

		Us.get("" + k + " " + n).add(U);
		return U;
	}
	
	private double[] calcXp(int k, int n, double time_in_ms, Job[] jobs) {
		double[] Xp = new double[k+4];

		long[][] job_times_on_server = new long [n][k+4];
		for (int i = 0; i < n; i++)
			job_times_on_server[i] = jobs[i].get_Job_times_on_server();

		for (int i = 0; i < (k+4); i++) {
			long sum_job_times_on_server = 0;
			for (int j = 0; j < n; j++)
				sum_job_times_on_server += job_times_on_server[j][i];
			Xp[i] = sum_job_times_on_server * 1000 / time_in_ms;
		}

		Xps.get("" + k + " " + n).add(Xp);
		return Xp;
	}
	
	private double[] calcJ(int k, int n, double[][] number_of_jobs_on_server) {
		double[] J = new double[k+4];

		double[][] pro = new double[k+4][n+1];
		for (int i = 0; i < (k+4); i++) {
			double sum = 0;
			for (int j = 0; j < (n+1); j++)
				sum += number_of_jobs_on_server[i][j];
			for (int j = 0; j < (n+1); j++)
				pro[i][j] = number_of_jobs_on_server[i][j]/sum;
		}
		
		for (int i = 0; i < (k+4); i++) {
			for (int j = 1; j <= n; j++)
				J[i] += j * pro[i][j];
		}

		Js.get("" + k + " " + n).add(J);
		return J;
	}
	
	private double calcT(int k, int n, Job[] jobs) {
		double T = 0;

		double temp = 0;
		int num = 0;
		for (int i = 0; i < jobs.length; i++) {
			for (double j: jobs[i].get_Cycle_durations())
				temp += j;
			num += jobs[i].get_Cycle_durations().size();
		}
		T = temp / (num * 1.0);

		Ts.get("" + k + " " + n).add(T);
		return T;
	}

	void calcAverageResults(int k, int n, Writer writer) throws IOException {
		LinkedList<double[]> U_list = Us.get("" + k + " " + n);
		double[] U = new double[k+4];
		for (double[] elem: U_list)
			for (int i = 0; i < (k+4); i++)
				U[i] += elem[i];
		
		LinkedList<double[]> Xp_list = Xps.get("" + k + " " + n);
		double[] Xp = new double[k+4];
		for (double[] elem: Xp_list)
			for (int i = 0; i < (k+4); i++)
				Xp[i] += elem[i];
		
		LinkedList<double[]> J_list = Js.get("" + k + " " + n);
		double[] J = new double[k+4];
		for (double[] elem: J_list)
			for (int i = 0; i < (k+4); i++)
				J[i] += elem[i];
		
		LinkedList<Double> T_list = Ts.get("" + k + " " + n);
		double T = 0;
		for (double elem: T_list)
			T += elem;
		
		for (int i = 0; i < (k+4); i++) {
			U[i] /= U_list.size();
			Xp[i] /= Xp_list.size();
			J[i] /= J_list.size();
		}
		T /= T_list.size();
		
		writer.write(printArray(U, "U"));
		writer.write(printArray(Xp, "X"));
		writer.write(printArray(J, "J"));
		writer.write("T = " + String.format("%.3f", T));
	}
	
	private String printArray(double[] array, String name) {
		StringBuilder sb = new StringBuilder(""); //"Array " + name + ":\n"
		for (int i = 0; i < array.length; i++) {
			sb.append(name + i + " = ");
			sb.append(String.format("%.3f", array[i]));
			sb.append("\n");
		}
		sb.append("\n");
		//System.out.println(sb.toString());
		return sb.toString();
	}

	private Job getFirstFromQueue(int server) {
		if (queue.get(server).isEmpty())
			return null;
		else
			return queue.get(server).getFirst();
	}

	private void addToQueue(Job job, double time_arrived) {
		queue.get(job.getServer()).add(job);
		job.setTime_arrived(time_arrived);
	}

	private void removeFirstFromQueueAndAddToQueue(Job job, double current_time) {
		queue.get(job.getServer()).removeFirst();
		int old_server = job.getServer();
		job.setNextServer();
		if (old_server == 0 && job.getServer() == 0)
			job.end_cycle(current_time);
		if (current_time > 0)
			addToQueue(job, current_time);
	}
	
	public double getNextRandomValue() {
		return rand.nextDouble();
	}
	
	public double getExponentialDistributionSample(int server) {
		/*if (server < 4)
			return -s[server] * Math.log(rand.nextDouble());
		else
			return -s[4] * Math.log(rand.nextDouble());*/
		if (server < 4)
			return ed[server].sample();
		else 
			return ed[4].sample();
	}

}
