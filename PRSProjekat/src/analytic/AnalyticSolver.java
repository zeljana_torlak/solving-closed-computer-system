package analytic;

import java.io.IOException;
import java.io.Writer;
import java.util.LinkedList;

import org.ejml.simple.SimpleMatrix;

public class AnalyticSolver {
	static final double[] s = { 5.0, 20.0, 15.0, 15.0, 20.0 }; //ms
	
	private LinkedList<SimpleMatrix> Xs = new LinkedList<SimpleMatrix>();
	
	void calcX(int k, Writer writer) throws IOException {
		int width = k+4;
		
		SimpleMatrix P = new SimpleMatrix(initMatrixP(k));
		SimpleMatrix U = new SimpleMatrix(initMatrixU(k));
		SimpleMatrix K = P.transpose().minus(SimpleMatrix.identity(width)).mult(U);
		
		SimpleMatrix X = K.svd().nullSpace();
		X = X.divide(X.get(0)); //normalization
		Xs.add(X);
		writer.write(printSimpleMatrix(X, "X"));
	}
	
	void calcResults(int k, int n, Writer writer) throws IOException {
		SimpleMatrix X = Xs.get(k-Test.K_MIN);
		double[] G = calcG(k, n, X);
		
		double[] U = calcU(k, n, X, G);
		writer.write(printArray(U, "U"));
		
		double[] Xp = calcXp(k, U); //poslovi u sekundi
		writer.write(printArray(Xp, "X"));
		
		double[] J = calcJ(k, n, X, G);
		writer.write(printArray(J, "J"));
		
		double Xsis = 0.1 * U[0]/s[0]; //P[0][0] = 0.1
		double T = n/Xsis;
		writer.write("T = " + String.format("%.3f [ms]", T));
	}

	private double[][] initMatrixP(int k){
		double[][] P = new double[k+4][k+4];
		
		P[0][0] = 0.1; //procesor je centralno server
		P[0][1] = P[0][2] = P[0][3] = 0.1; //pristup sistemskim diskovima sa procesora
		for (int i = 0; i < k; i++) //pristup korisnickim diskovima sa procesora
			P[0][i+4] = 60/(k * 100.0);
		
		P[1][0] = P[2][0] = P[3][0] = 0.4; //pristup procesoru sa sistemskih diskova
		for (int i = 0; i < k; i++)  //pristup korisnickim diskovima sa sistemskih diskova
			P[1][i+4] = P[2][i+4] = P[3][i+4] = 60/(k * 100.0);
		
		for (int i = 0; i < k; i++)  //pristup procesoru sa korisnickih diskova
			P[i+4][0] = 1;
		
		return P;
	}
	
	private double[][] initMatrixU(int k){
		double[][] U = new double[k+4][k+4]; //ms
		
		for (int i = 0; i < 4; i++)
			U[i][i] = 1000/s[i];
		for (int i = 0; i < k; i++)
			U[i+4][i+4] = 1000/s[4];
		
		return U;
	}
	
	private double[] calcG(int k, int n, SimpleMatrix X) {
		double[] G = new double[n+1];
		G[0] = 1;
		
		for (int j = 0; j < X.getNumElements(); j++)
			for (int i = 1; i < G.length; i++)
				G[i] = G[i] + (G[i-1] * X.get(j));
		
		return G;
	}
	
	private double[] calcU(int k, int n, SimpleMatrix X, double[] G) {
		double[] U = new double[k+4];
		for (int i = 0; i < (k+4); i++)
			U[i] = X.get(i) * G[n-1]/G[n];
		
		return U;
	}
	
	private double[] calcXp(int k, double[] U) {
		double[] Xp = new double[k+4];
		for (int i = 0; i < 4; i++)
			Xp[i] = U[i]*1000/s[i];
		for (int i = 4; i < (k+4); i++)
			Xp[i] = U[i]*1000/s[4];
		
		return Xp;
	}
	
	private double[] calcJ(int k, int n, SimpleMatrix X, double[] G) {
		double[] J = new double[k+4];
		for (int i = 0; i < J.length; i++) {
			for (int j = 1; j <= n ; j++)
				J[i] += Math.pow(X.get(i), j) * G[n-j]/G[n];
		}
		
		return J;
	}

	public static String printMatrix(double[][] matrix, String name) {
		StringBuilder sb = new StringBuilder("Matrix " + name + ":\n");
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++)
				sb.append(String.format("%1.3f ", matrix[i][j]));
			sb.append("\n");
		}
		sb.append("\n");
		//System.out.println(sb.toString());
		return sb.toString();
	}
	
	private String printArray(double[] array, String name) {
		StringBuilder sb = new StringBuilder(""); //"Array " + name + ":\n"
		for (int i = 0; i < array.length; i++) {
			sb.append(name + i + " = ");
			sb.append(String.format("%.3f ", array[i]));
			sb.append("\n");
		}
		sb.append("\n");
		//System.out.println(sb.toString());
		return sb.toString();
	}
	
	private String printSimpleMatrix(SimpleMatrix matrix, String name) {
		StringBuilder sb = new StringBuilder("Matrix " + name + ":\n");
		for (int i = 0; i < matrix.numRows(); i++) {
			for (int j = 0; j < matrix.numCols(); j++)
				sb.append(String.format("%1.3f", matrix.get(i, j)));
			sb.append("\n");
		}
		sb.append("\n");
		//System.out.println(sb.toString());
		return sb.toString();
	}
	
}
