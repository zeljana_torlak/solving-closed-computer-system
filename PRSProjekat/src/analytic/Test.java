package analytic;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

public class Test {
	static final int K_MIN = 2;
	static final int K_MAX = 8;
	static final int[] N = { 10, 15, 20 };

	public static void main(String[] args) {
		AnalyticSolver as = new AnalyticSolver();
		
		try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("potraznje_analiticki.txt")))){
			for (int k = K_MIN; k <= K_MAX; k++) {
				writer.write("----------------K=" + k + "----------------\n");
				as.calcX(k, writer);
				writer.write("\n\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("rezultati_analiticki.txt")))){
			for (int n: N)
				for (int k = K_MIN; k <= K_MAX; k++) {
					writer.write("----------------K=" + k + ", N=" + n + "----------------\n");
					as.calcResults(k, n, writer);
					writer.write("\n\n");
				}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("Analytical solving successfully completed!");

	}

}
